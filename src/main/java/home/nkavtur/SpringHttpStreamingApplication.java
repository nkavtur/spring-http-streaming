package home.nkavtur;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
import java.util.stream.IntStream;

@SpringBootApplication
public class SpringHttpStreamingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringHttpStreamingApplication.class, args);
    }

    @Data
    @AllArgsConstructor
    class User {
        private UUID id;
        private String name;
    }

    @RestController
    @RequestMapping("/users")
    class UserController {

        @GetMapping(value = "/http-stream")
        public ResponseBodyEmitter httpStream() {
            ResponseBodyEmitter sink = new ResponseBodyEmitter();
            new Thread(() -> {
                for (int i = 0; i < 100; i++) {
                    User user = new User(UUID.randomUUID(), String.valueOf(i));
                    try {
                        System.out.println(i);
                        sink.send(user);
                        Thread.sleep(100);
                    } catch (Exception e) {
                        sink.completeWithError(e);
                    }
                }
                sink.complete();
            }).start();


            return sink;
        }

        @GetMapping(value = "/sse-stream")
        public SseEmitter sseStream() {
            SseEmitter sink = new SseEmitter();
            new Thread(() -> {
                for (int i = 0; i < 100; i++) {
                    User user = new User(UUID.randomUUID(), String.valueOf(i));
                    try {
                        sink.send(user);
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        sink.completeWithError(e);
                    }
                }
                sink.complete();
            }).start();

            return sink;
        }
    }

}
